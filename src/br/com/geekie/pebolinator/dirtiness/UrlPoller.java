package br.com.geekie.pebolinator.dirtiness;

import br.com.geekie.pebolinator.models.Match;
import br.com.geekie.pebolinator.web.DataUploader;
import br.com.geekie.pebolinator.web.DataUploaderListener;

public class UrlPoller extends Thread {

    private boolean isAlive;
    private Match currentMatch;
    private DataUploaderListener webListener;

    public UrlPoller(Match currentMatch, DataUploaderListener webListener) {
        super();
        this.currentMatch = currentMatch;
        this.webListener = webListener;
    }

    @Override
    public void run() {
        isAlive = true;
        while (isAlive) {
            DataUploader.canMatchStart(currentMatch, webListener);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
            }
        }

    }

    public void stopPolling() {
        isAlive = false;
        this.interrupt();
    }

}
