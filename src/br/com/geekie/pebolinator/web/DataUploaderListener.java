package br.com.geekie.pebolinator.web;


public interface DataUploaderListener {

    /**
     * Receives the notification that a new match has been created at the server.
     */
    public void onNewMatchCreated();

    /**
     * Receives the notification that the match creation with the server has failed.
     */
    public void onNewMatchFail();

    /**
     * Receives the notification that the match has been uploaded.
     */
    public void onMatchUploaded();

    /**
     * Receives the notification that the match upload has failed.
     */
    public void onMatchUploadFail();

    /**
     * Receives the result of DataUploader.canMatchStart();
     * 
     * @param canStart
     */
    public void onStartMatchQueryCompleted(boolean canStart);

    /**
     * Receives the notification that the query has failed.
     */
    public void onStartMatchQueryFail();

}
