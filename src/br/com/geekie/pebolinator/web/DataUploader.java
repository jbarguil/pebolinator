package br.com.geekie.pebolinator.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;
import br.com.geekie.pebolinator.models.Match;

/**
 * Deals with server communication.
 * 
 * @author jbarguil
 */
public class DataUploader {

    private static final String HOME_URL = "http://pebostats.herokuapp.com";
    private static final String MATCH_POST_URL = HOME_URL + "/match";
    private static final String CAN_MATCH_START_GET_URL = HOME_URL + "/can_start";

    private static final String TAG = "DataUploader";

    /**
     * Creates a new match. Runs in a separate Thread.
     * 
     * @param match
     * @param callback
     * @return immediately, result is given on callback
     */
    public static void newMatch(Match match, final DataUploaderListener callback) {
        final String json = match.toString();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    post(MATCH_POST_URL, json);
                } catch (ClientProtocolException e) {
                    callback.onNewMatchFail();
                } catch (IOException e) {
                    callback.onNewMatchFail();
                }
                callback.onNewMatchCreated();
            }
        }).start();
    }

    /**
     * Posts a match. Runs in a separate Thread.
     * 
     * @param match
     * @param callback
     * @return immediately, result is given on callback
     */
    public static void postMatch(Match match, final DataUploaderListener callback) {
        final String json = match.toString();
        
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    post(MATCH_POST_URL, json);
                } catch (ClientProtocolException e) {
                    callback.onMatchUploadFail();
                } catch (IOException e) {
                    callback.onMatchUploadFail();
                }
                callback.onMatchUploaded();
            }
        }).start();
    }

    /**
     * Checks if a new match can be started. Runs on a separate Thread.
     * 
     * @param match
     * @param callback
     * @return immediately, result is given on callback
     */
    public static void canMatchStart(Match match, final DataUploaderListener callback) {
        final String url = CAN_MATCH_START_GET_URL + "?match_id=" + match.getId();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response = get(url);
                    if (response.equals("True")) {
                        callback.onStartMatchQueryCompleted(true);
                    }
                } catch (ClientProtocolException e) {
                    callback.onStartMatchQueryFail();
                } catch (IOException e) {
                    callback.onStartMatchQueryFail();
                }

                callback.onStartMatchQueryCompleted(false);
            }
        });
    }

    private static String post(String url, String json) throws ClientProtocolException, IOException {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        // Add your data
        List<NameValuePair> data = new ArrayList<NameValuePair>(2);
        data.add(new BasicNameValuePair("data", json));
        httppost.setEntity(new UrlEncodedFormEntity(data));

        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);
        String s = inputStreamToString(response.getEntity().getContent());

        Log.i(TAG, "POST to url " + url);
        Log.i(TAG, "data: " + data);
        Log.i(TAG, "json: " + json);
        Log.i(TAG, "response: " + s);

        return null;
    }

    private static String get(String url) throws ClientProtocolException, IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response = httpclient.execute(new HttpGet(url));
        String s = inputStreamToString(response.getEntity().getContent());

        Log.i(TAG, "GET to url " + url);
        Log.i(TAG, "response: " + s);

        return s;
    }

    private static String inputStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) {
            total.append(line);
        }

        // Return full string
        return total.toString();
    }
}

